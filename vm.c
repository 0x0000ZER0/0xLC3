#include <stddef.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/termios.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#define MEM_MAX (1 << 16)
uint16_t mem[MEM_MAX];

enum
{
    R_R0 = 0,
    R_R1,
    R_R2,
    R_R3,
    R_R4,
    R_R5,
    R_R6,
    R_R7,
    R_PC, 
    R_COND,
    R_COUNT
};

uint16_t reg[R_COUNT];

enum
{
    OP_BR = 0, /* branch */
    OP_ADD,    /* add  */
    OP_LD,     /* load */
    OP_ST,     /* store */
    OP_JSR,    /* jump register */
    OP_AND,    /* bitwise and */
    OP_LDR,    /* load register */
    OP_STR,    /* store register */
    OP_RTI,    /* unused */
    OP_NOT,    /* bitwise not */
    OP_LDI,    /* load indirect */
    OP_STI,    /* store indirect */
    OP_JMP,    /* jump */
    OP_RES,    /* reserved (unused) */
    OP_LEA,    /* load effective address */
    OP_TRAP    /* execute trap */
};

enum
{
    FL_POS = 1 << 0, /* P */
    FL_ZRO = 1 << 1, /* Z */
    FL_NEG = 1 << 2, /* N */
};

enum
{
    MR_KBSR = 0xFE00, /* keyboard status */
    MR_KBDR = 0xFE02  /* keyboard data */
};

uint16_t
sign_ext(uint16_t x, size_t c)
{
	if ((x >> (c - 1)) & 1) 
		x |= 0xFF << c;
	return x;
}

void
flag_set(uint16_t r)
{
	if (r == 0)
		reg[R_COND] = FL_ZRO;
	else if ((r >> 15) & 1)
		reg[R_COND] = FL_NEG;
	else
		reg[R_COND] = FL_POS;
}

enum
{
    TRAP_GETC 	= 0x20, /* get character from keyboard, not echoed onto the terminal */
    TRAP_OUT 	= 0x21, /* output a character */
    TRAP_PUTS 	= 0x22, /* output a word string */
    TRAP_IN 	= 0x23, /* get character from keyboard, echoed onto the terminal */
    TRAP_PUTSP 	= 0x24, /* output a byte string */
    TRAP_HALT 	= 0x25  /* halt the program */
};

uint16_t
mem_get(uint16_t addr)
{
	if (addr == MR_KBSR) {
    		fd_set readfds;
    		FD_ZERO(&readfds);
    		FD_SET(STDIN_FILENO, &readfds);

    		struct timeval timeout;
    		timeout.tv_sec = 0;
    		timeout.tv_usec = 0;

		int res;
		res = select(1, &readfds, NULL, NULL, &timeout);
		if (res != 0) {
			mem[MR_KBDR] = 1 << 15;
			mem[MR_KBDR] = getchar();
		} else {
			mem[MR_KBDR] = 0;
		}
	}

	return mem[addr];
}

void
mem_put(uint16_t addr, uint16_t val)
{
	mem[addr] = val;
}

struct termios old_tio;

void 
irq(int sig)
{
	(void)sig;
	tcsetattr(STDIN_FILENO, TCSANOW, &old_tio);
	exit(2);
}

int
main(int argc, char *argv[])
{
	if (argc < 2) {
		printf("INF: usage \t lc3-vm <bin>\n");
		return 1;
	}

	signal(SIGINT, irq);

	tcgetattr(STDIN_FILENO, &old_tio);
	
	struct termios new_tio;
	new_tio = old_tio;
	new_tio.c_lflag &= ~ICANON & ~ECHO;
	
	tcsetattr(STDIN_FILENO, TCSANOW, &new_tio);

	int file;
	file = open(argv[1], O_RDONLY);
	printf("INF: opened the '%s'\n", argv[1]);

	uint16_t orig;
	read(file, &orig, sizeof (orig));
#ifdef __ORDER_LITTLE_ENDIAN__
	orig = __builtin_bswap16(orig);
#endif
	printf("INF: the code section starts at [0x%08hX]\n", orig);

	ssize_t mem_max;
	mem_max = (MEM_MAX - orig);

	uint16_t *mem_ptr;
	mem_ptr = mem + orig;

	size_t req;
	req = 1024;

	ssize_t bytes;
	bytes = read(file, mem_ptr, req);
	while (bytes > 0) {
		printf("INF: read [%li] bytes, requested [%lu] bytes\n", bytes, req); 

		mem_max -= bytes;
		mem_ptr += bytes;

		bytes = read(file, mem_ptr, req);
	}

#ifdef __ORDER_LITTLE_ENDIAN__
	uint16_t *ptr;
	ptr = mem + orig;
	while (bytes > 0) {
		*ptr = __builtin_bswap16(*ptr);
		bytes -= sizeof (uint16_t);
	}
#endif

	close(file);

	reg[R_COND] = FL_ZRO;
	reg[R_PC] = orig;

	int busy;
	busy = 1;
	while (busy) {
		uint16_t inst;
		inst = mem_get(reg[R_PC]);

		++reg[R_PC];

		uint16_t op;
		op = inst >> 12;
		switch (op) {
		case OP_BR:
		{
#ifdef DEBUG
			printf("DBG: BR [0x%08hX]\n", reg[R_PC]);
#endif
			uint8_t nzp;
			nzp = (inst >> 0x9) & 0x7;

			uint16_t pc_off9;
			pc_off9 = inst & 0x1FF;

			uint16_t s_pc_off9;
			s_pc_off9 = sign_ext(pc_off9, 9);

			if (nzp & reg[R_COND])
				reg[R_PC] += s_pc_off9;

			break;
		}
		case OP_ADD:
		{
#ifdef DEBUG
			printf("DBG: ADD [0x%08hX]\n", reg[R_PC]);
#endif
			uint16_t dr;
			dr = (inst >> 9) & 0x7;

			uint16_t sr1;
			sr1 = (inst >> 6) & 0x7;

			uint8_t m;
			m = (inst >> 5) & 0x1;
			if (m) {
				uint8_t imm5;
				imm5 = inst & 0x1F;

				uint16_t s_imm5;
				s_imm5 = sign_ext(imm5, 5);

				reg[dr] = reg[sr1] + s_imm5;
			} else {
				uint16_t sr2;
				sr2 = inst & 0x7;

				reg[dr] = reg[sr1] + reg[sr2];
			}
			flag_set(reg[dr]);

			break;
		}
		case OP_LD:  
		{
#ifdef DEBUG
			printf("DBG: LD  [0x%08hX]\n", reg[R_PC]);
#endif
			uint16_t dr;
			dr = (inst >> 9) & 0x7;

			uint16_t pc_off9;
			pc_off9 = inst & 0x1FF;

			uint16_t s_pc_off9;
			s_pc_off9 = sign_ext(pc_off9, 9);

			reg[dr] = mem_get(reg[R_PC] + s_pc_off9);
			flag_set(reg[dr]);	

			break;
		}
                case OP_ST:  
		{
#ifdef DEBUG
			printf("DBG: ST [0x%08hX]\n", reg[R_PC]);
#endif
			uint16_t sr;
			sr = (inst >> 9) & 0x7;

			uint16_t pc_off9;
			pc_off9 = inst & 0x1FF;

			uint16_t s_pc_off9;
			s_pc_off9 = sign_ext(pc_off9, 9);

			mem_put(reg[R_PC] + s_pc_off9, reg[sr]);

			break;
		}
                case OP_JSR: 
		{
#ifdef DEBUG
			printf("DBG: JSR\n");
#endif
			uint8_t m;
			m = (inst >> 0xB) & 0x1;

			if (m) {
				uint16_t pc_off11;
				pc_off11 = inst & 0x7FF;

				uint16_t s_pc_off11;
				s_pc_off11 = sign_ext(pc_off11, 11);

				reg[R_PC] += s_pc_off11;
			} else {
				uint16_t base_r;
				base_r = (inst >> 0x6) & 0x7;

				reg[R_PC] = reg[base_r];
			}

			break;
		}
                case OP_AND: 
		{
#ifdef DEBUG
			printf("DBG: AND\n");
#endif
			uint16_t dr;
			dr = (inst >> 9) & 0x7;

			uint16_t sr1;
			sr1 = (inst >> 6) & 0x7;

			uint8_t m;
			m = (inst >> 5) & 0x1;
			if (m) {
				uint8_t imm5;
				imm5 = inst & 0x1F;

				uint16_t s_imm5;
				s_imm5 = sign_ext(imm5, 5);

				reg[dr] = reg[sr1] & s_imm5;
			} else {
				uint16_t sr2;
				sr2 = inst & 0x7;

				reg[dr] = reg[sr1] & reg[sr2];
			}
			flag_set(reg[dr]);

			break;
		}
                case OP_LDR: 
		{
#ifdef DEBUG
			printf("DBG: LDR\n");
#endif
			uint16_t dr;
			dr = (inst >> 9) & 0x7;

			uint16_t base_r;
			base_r = (inst >> 6) & 0x7;

			uint16_t pc_off6;
			pc_off6 = inst & 0x3F;

			uint16_t s_pc_off6;
			s_pc_off6 = sign_ext(pc_off6, 6);

			reg[dr] = mem_get(reg[base_r] + s_pc_off6);
			flag_set(reg[dr]);

			break;
		}
                case OP_STR: 
		{
#ifdef DEBUG
			printf("DBG: STR\n");
#endif
			uint16_t sr;
			sr = (inst >> 9) & 0x7;

			uint16_t base_r;
			base_r = (inst >> 6) & 0x7;

			uint16_t pc_off6;
			pc_off6 = inst & 0x3F;

			uint16_t s_pc_off6;
			s_pc_off6 = sign_ext(pc_off6, 6);

			mem_put(reg[base_r] + s_pc_off6, reg[sr]);

			break;
		}
                case OP_RTI: 
#ifdef DEBUG
			printf("DBG: RTI [0x%08hX]\n", reg[R_PC]);
#endif
			abort();
			break;
                case OP_NOT: 
		{
#ifdef DEBUG
			printf("DBG: NOT\n");
#endif
			uint16_t dr;
			dr = (inst >> 9) & 0x7;

			uint16_t sr;
			sr = (inst >> 6) & 0x7;

			reg[dr] = ~reg[sr];
			flag_set(reg[dr]);

			break;
		}
                case OP_LDI: 
		{
#ifdef DEBUG
			printf("DBG: LDI\n");
#endif
			uint16_t dr;
			dr = (inst >> 0x9) & 0x7;

			uint16_t pc_off9;
			pc_off9 = inst & 0x1FF;

			uint16_t s_pc_off9;
			s_pc_off9 = sign_ext(pc_off9, 9);

			uint16_t mem_addr;
			mem_addr = mem_get(reg[R_PC] + s_pc_off9);

			reg[dr] = mem_get(mem_addr);
			flag_set(reg[dr]);
			break;
		}
                case OP_STI: 
		{
#ifdef DEBUG
			printf("DBG: STI\n");
#endif
			uint16_t sr;
			sr = (inst >> 9) & 0x7;

			uint16_t pc_off9;
			pc_off9 = inst & 0x1FF;

			uint16_t s_pc_off9;
			s_pc_off9 = sign_ext(pc_off9, 9);

			uint16_t mem_addr;
			mem_addr = mem_get(reg[R_PC] + s_pc_off9);

			mem_put(mem_addr, reg[sr]);
			
			break;
		}
                case OP_JMP: 
		{
#ifdef DEBUG
			printf("DBG: JMP\n");
#endif
			uint16_t base_r;
			base_r = (inst >> 0x6) & 0x7;

			reg[R_PC] += reg[base_r];

			break;
		}
                case OP_RES:
#ifdef DEBUG
			printf("DBG: RES [0x%08hX]\n", reg[R_PC]);
#endif
			abort();
			break;
                case OP_LEA: 
		{
#ifdef DEBUG
			printf("DBG: LEA\n");
#endif
			uint16_t dr;
			dr = (inst >> 0x9) & 0x7;

			uint16_t pc_off9;
			pc_off9 = inst & 0x1FF;

			uint16_t s_pc_off9;
			s_pc_off9 = sign_ext(pc_off9, 9);

			reg[dr] = reg[R_PC] + s_pc_off9;
			flag_set(reg[dr]);

			break;
		}
		case OP_TRAP:
		{
#ifdef DEBUG
			printf("DBG: TRAP -> ");
#endif
			reg[R_R7] = reg[R_PC];

			uint16_t vec;
			vec = inst & 0xFF;
			switch (vec) {
			case TRAP_GETC:
#ifdef DEBUG
				printf("GETC\n");
#endif
				reg[R_R0] = getchar();
				flag_set(reg[R_R0]);
				break;
			case TRAP_OUT:
#ifdef DEBUG
				printf("OUT\n");
#endif
				putc((char)reg[R_R0], stdout);
				fflush(stdout);	

				break;
			case TRAP_PUTS:
			{
#ifdef DEBUG
				printf("PUTS\n");
#endif
				uint16_t *c;
				c = mem + reg[R_R0];
				while (*c++)
					putc(*c, stdout);
				fflush(stdout);	

				break;
			}
			case TRAP_IN:
			{
#ifdef DEBUG
				printf("IN\n");
#endif
				printf("> ");
				char c;
				c = getchar();

				putc(c, stdout);
				fflush(stdout);

				reg[R_R0] = (uint16_t)c;
				flag_set(reg[R_R0]);
				break;
			}
			case TRAP_PUTSP:
			{
#ifdef DEBUG
				printf("PUTSP\n");
#endif
				uint16_t *c;
				c = mem + reg[R_R0];
				while (*c++) {
					char c1;
					c1 = *c & 0xFF;
					putc(c1, stdout);

					char c2;
					c2 = *c >> 0x8;
					putc(c2, stdout);
				}
				fflush(stdout);
				break;
			}
			case TRAP_HALT:
#ifdef DEBUG
				printf("HALT\n");
#endif
				printf("terminating\n");
				busy = 0;
				break;
			}

			break;
		}
		default:
			printf("INF: unknown op code.\n");
			break;
		}
	}

	tcsetattr(STDIN_FILENO, TCSANOW, &old_tio);
	return 0;
}
